<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Entity\Owner;
use App\Repository\OwnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private OwnerRepository $ownerRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param OwnerRepository $ownerRepository
     */
    public function __construct(EntityManagerInterface $entityManager, OwnerRepository $ownerRepository)
    {
        $this->entityManager = $entityManager;
        $this->ownerRepository = $ownerRepository;
    }


    #[Route('/', name: 'home')]
    public function index(): Response
    {
        $ownerEntity = new Owner();
        $ownerEntity->setFirstName('Jules');
        $ownerEntity->setLastName('Pauly');
        $ownerEntity->setBirthDate(new \DateTime('1990-12-12'));

        $myVariable = 'hello';

        dump($myVariable);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'myVariable' => $myVariable,
            'ownerEntity' => $ownerEntity
        ]);
    }

    #[Route('/test/{other_variable}/{again_variable}', name: 'test')]
    public function test(string $other_variable, string $again_variable)
    {

        dump($other_variable, $again_variable);

        return $this->render('home/test.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/test_2', name: 'test2')]
    public function test_2()
    {
        $ownerEntity = new Owner();
        $ownerEntity->setFirstName('Jules');
        $ownerEntity->setLastName('Pauly');
        $ownerEntity->setBirthDate(new \DateTime('1990-12-12'));

        $this->entityManager->persist($ownerEntity);
        $this->entityManager->flush();
//
        return $this->render('home/test_2.html.twig', [
            'controller_name' => 'HomeController',
//            'animalEntity' => $animalEntity
            'ownerEntity' => $ownerEntity
        ]);
    }

    #[Route('/test_select', name: 'test_select')]
    public function test_select()
    {
        //findAll -> Permet de récupérer toutes les entitées
        $ownerEntities = $this->ownerRepository->findAll();

        //find -> permet de récupétrer par son id
//        $ownerEntity = $this->ownerRepository->find(1);


//        dump($ownerEntities);

        return $this->render('home/select.html.twig', [
            'ownerEntities' => $ownerEntities,
//            'ownerEntity' => $ownerEntity
        ]);
    }

    #[Route('/owner_list', name: 'owner_list')]
    public function ownerList()
    {
        $ownerEntities = $this->ownerRepository->findAll();
        return $this->render('home/owner-list.html.twig', [
            'ownerEntities' => $ownerEntities
        ]);
    }

    #[Route('/owner_item/{id}', name: 'owner_item')]
    public function ownerItem(string $id)
    {
        $ownerEntity = $this->ownerRepository->find($id);
        return $this->render('home/owner-item.html.twig', [
            'ownerEntity' => $ownerEntity
        ]);
    }
}
